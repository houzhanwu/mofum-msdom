package com.mofum.msdom.excel.pager;

import java.util.List;

/**
 * 分页回调函数
 *
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 15:53
 */
public interface PageCallback<Data, Context extends PageContext> {

    /**
     * 回调函数
     *
     * @param context 分页上下文
     *  分页数据
     */
    List<Data> call(Context context);
}
