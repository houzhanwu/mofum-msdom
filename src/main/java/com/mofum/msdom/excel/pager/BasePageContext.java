package com.mofum.msdom.excel.pager;

import com.mofum.msdom.excel.metadata.MPSheet;

/**
 * 默认的分页上下文
 *
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 15:53
 */
public class BasePageContext implements PageContext {

    /**
     * 页面大小
     */
    private int pageSize;

    /**
     * 当前页
     */
    private int pageNumber;

    /**
     * 总页数
     */
    private int totalPages;

    /**
     * 总记录条数
     */
    private int totalRecords;

    /**
     * 工作表
     */
    private MPSheet[] sheets;

    /**
     * 工作表索引
     */
    private int sheetIndex;

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(int totalRecords) {
        this.totalRecords = totalRecords;
    }

    public MPSheet[] getSheets() {
        return sheets;
    }

    public void setSheets(MPSheet[] sheets) {
        this.sheets = sheets;
    }

    public int getSheetIndex() {
        return sheetIndex;
    }

    public void setSheetIndex(int sheetIndex) {
        this.sheetIndex = sheetIndex;
    }
}
