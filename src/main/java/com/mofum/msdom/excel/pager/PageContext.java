package com.mofum.msdom.excel.pager;

import com.mofum.msdom.excel.metadata.MPSheet;

/**
 * 分页上下文
 *
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 15:53
 */
public interface PageContext {

    public int getPageSize();

    public void setPageSize(int pageSize);

    public int getPageNumber();

    public void setPageNumber(int pageNumber);

    public int getTotalPages();

    public void setTotalPages(int totalPages);

    public int getTotalRecords();

    public void setTotalRecords(int totalRecords);

    public MPSheet[] getSheets();

    public void setSheets(MPSheet[] sheets);

    public int getSheetIndex();

    public void setSheetIndex(int sheetIndex);
}
