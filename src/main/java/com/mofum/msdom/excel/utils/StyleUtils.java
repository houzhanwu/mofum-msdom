package com.mofum.msdom.excel.utils;

import com.mofum.msdom.excel.annotation.parser.impl.ATStyleParser;
import com.mofum.msdom.excel.metadata.MPFont;
import com.mofum.msdom.excel.metadata.MPStyle;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 1615690513@qq.com
 * @since 2018/11/30 0030 11:53
 */
public class StyleUtils {

    public static MPStyle createStyle(String style) {
        MPStyle mpStyle = new MPStyle();

        Map<String, Map<String, String>> map = StyleUtils.groupValues(style);

        MPFont font = StyleUtils.parseFont(map.get("font"));

        mpStyle.setFont(font);

        StyleUtils.parseStyle(mpStyle, map.get("msdom"));

        return mpStyle;
    }

    public static void parseStyle(MPStyle mpStyle, Map<String, String> msdom) {
        if (msdom == null) {
            return;
        }
        String wrapText = msdom.get("wrap-text");
        if (wrapText != null) {
            mpStyle.setWrapText(Boolean.parseBoolean(wrapText));
        }

        String backgroundColor = msdom.get("background-color");
        if (backgroundColor != null) {
            mpStyle.setFillBackgroundColor(backgroundColor);
        }

        String background = msdom.get("background");
        if (background != null) {
            String[] args = background.split("\\s");

            switch (args.length) {
                case 1:
                    mpStyle.setFillBackgroundColor(args[0]);
                    break;
                case 2:
                    mpStyle.setFillBackgroundColor(args[0]);
                    mpStyle.setFillPattern(args[1]);
                    break;
                default:
                    mpStyle.setFillBackgroundColor(background);
                    break;
            }

        }

        String foregroundColor = msdom.get("foreground-color");
        if (foregroundColor != null) {
            mpStyle.setFillForegroundColor(foregroundColor);
        }

        String foreground = msdom.get("foreground");

        if (foreground != null) {
            String[] args = foreground.split("\\s");

            switch (args.length) {
                case 1:
                    mpStyle.setFillForegroundColor(args[0]);
                    break;
                case 2:
                    mpStyle.setFillForegroundColor(args[0]);
                    mpStyle.setFillPattern(args[1]);
                    break;
                default:
                    mpStyle.setFillForegroundColor(foreground);
                    break;
            }

        }

        String borderBottom = msdom.get("border-bottom");
        if (borderBottom != null) {
            mpStyle.setBorderBottom(borderBottom);
        }

        String border = msdom.get("border");
        if (border != null) {
            String[] args = border.trim().split("\\s");

            if (args.length > 0) {
                mpStyle.setBorderTop(args[0]);
                mpStyle.setBorderLeft(args[0]);
                mpStyle.setBorderRight(args[0]);
                mpStyle.setBorderBottom(args[0]);
            }
            if (args.length > 1) {
                mpStyle.setBorderTopColor(args[1]);
                mpStyle.setBorderLeftColor(args[1]);
                mpStyle.setBorderRightColor(args[1]);
                mpStyle.setBorderBottomColor(args[1]);
            }

        }

        String borderTop = msdom.get("border-top");
        if (borderTop != null) {
            mpStyle.setBorderTop(borderTop);
        }

        String borderLeft = msdom.get("border-left");
        if (borderLeft != null) {
            mpStyle.setBorderLeft(borderLeft);
        }

        String borderRight = msdom.get("border-right");
        if (borderRight != null) {
            mpStyle.setBorderRight(borderRight);
        }


        String borderColor = msdom.get("border-color");
        if (borderColor != null) {
            mpStyle.setBorderBottomColor(borderColor);
            mpStyle.setBorderTopColor(borderColor);
            mpStyle.setBorderRightColor(borderColor);
            mpStyle.setBorderLeftColor(borderColor);
        }

        String borderBottomColor = msdom.get("border-bottom-color");
        if (borderBottomColor != null) {
            mpStyle.setBorderBottomColor(borderBottomColor);
        }

        String borderTopColor = msdom.get("border-top-color");
        if (borderTopColor != null) {
            mpStyle.setBorderTopColor(borderTopColor);
        }

        String borderLeftColor = msdom.get("border-left-color");
        if (borderLeftColor != null) {
            mpStyle.setBorderLeftColor(borderLeftColor);
        }

        String borderRightColor = msdom.get("border-right-color");
        if (borderRightColor != null) {
            mpStyle.setBorderRightColor(borderRightColor);
        }

        String hidden = msdom.get("hidden");
        if (hidden != null) {
            mpStyle.setHidden(Boolean.parseBoolean(hidden));
        }

        String alignment = msdom.get("alignment");
        if (alignment != null) {
            mpStyle.setAlignment(alignment);
        }

        String verticalAlignment = msdom.get("vertical-alignment");
        if (verticalAlignment != null) {
            mpStyle.setVerticalAlignment(verticalAlignment);
        }

        String fillPattern = msdom.get("fill-pattern");
        if (fillPattern != null) {
            mpStyle.setFillPattern(fillPattern);
        }

        String dataFormat = msdom.get("data-format");
        if (dataFormat != null) {
            mpStyle.setDataFormat(dataFormat);
        }
    }

    public static MPFont parseFont(Map<String, String> font) {
        if (font == null) {
            return new MPFont();
        }

        MPFont mpFont = new MPFont();

        String bold = font.get("font-bold");

        if (bold != null) {
            mpFont.setBold(Boolean.parseBoolean(bold));
        }

        String name = font.get("font-name");
        if (name != null) {
            mpFont.setFontName(name);
        }

        String italic = font.get("font-italic");
        if (italic != null) {
            mpFont.setItalic(Boolean.parseBoolean(italic));
        }

        String color = font.get("font-color");
        if (color != null) {
            mpFont.setColor(color);
        }

        String height = font.get("font-height");

        if (height != null) {
            mpFont.setFontHeight(Short.parseShort(height));
        }

        String strikeout = font.get("font-strikeout");
        if (strikeout != null) {
            mpFont.setStrikeout(Boolean.parseBoolean(strikeout));
        }

        String underline = font.get("font-underline");
        if (underline != null) {
            mpFont.setUnderline(underline);
        }
        return mpFont;
    }

    public static Map<String, Map<String, String>> groupValues(String text) {
        if (text == null) {
            return new HashMap<>();
        }

        Map<String, Map<String, String>> group = new HashMap<>();

        Map<String, String> props = parseValues(text);

        for (String prop : props.keySet()) {

            String[] keys = prop.split("\\-");


            if (keys.length > 0) {

                switch (keys[0].trim().toLowerCase()) {
                    case "font":
                        break;
                    default:
                        keys[0] = "msdom";
                        break;

                }

            }

            Map<String, String> value = group.get(keys[0]);
            if (value == null) {
                value = new HashMap<>();
                group.put(keys[0], value);
            }
            value.put(prop, props.get(prop));

        }

        return group;

    }


    public static Map<String, String> parseValues(String text) {
        Map<String, String> map = new HashMap<>();
        String[] keyValues = text.split("\\;|\\||\\,");

        for (String keyValue : keyValues) {

            String[] part = keyValue.split("\\:|\\=");

            if (part != null) {
                if (part.length == 2) {
                    map.put(part[0], part[1].trim());
                }
            }

        }
        return map;
    }


    public static void main(String[] args) throws IOException {

        String style = "font-width=1;height=2;color=#fff;size=11;";

        Map<String, Map<String, String>> group = groupValues(style);

        System.out.println(group);


    }

}
