package com.mofum.msdom.excel.utils;

/**
 * @author 1615690513@qq.com
 * @since 2018/11/30 0030 09:42
 */
public class StringUtils {


    /**
     * 字符串长度
     *
     * @param str
     */
    public static int stringLength(String str) {
        int length = 0;
        if (str == null) {
            return 0;
        }

        char[] c = str.toCharArray();
        for (int i = 0; i < c.length; i++) {
            String len = Integer.toBinaryString(c[i]);
            length = length + len.length();
        }
        return length * 45;
    }

    public static void main(String[] args) {

        String str = null;

        System.out.println(stringLength(str));

    }
}
