package com.mofum.msdom.excel.utils;

public class ReflectUtils {

    public static <T> Class<T> convertToClass(T o) {
        Class clazz = null;

        if (o instanceof Class) {
            clazz = (Class) o;
        } else {
            clazz = o.getClass();
        }
        return clazz;
    }
}
