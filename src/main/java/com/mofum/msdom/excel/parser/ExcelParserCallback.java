package com.mofum.msdom.excel.parser;

import com.mofum.msdom.excel.constant.RowContext;

/**
 * 解析完成后的回调
 *
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 15:54
 */
public interface ExcelParserCallback<Data> {

    /**
     * 解析完毕后
     *
     * @param data 数据
     */
    void parseDataAfter(RowContext<Data> data);
}
