package com.mofum.msdom.excel.annotation;

import com.mofum.msdom.excel.metadata.MPHeaderColumn;

import java.lang.annotation.*;

/**
 * 头列
 *
 * @author 1615690513@qq.com
 * @since 2018/11/27 0027 13:58
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ATHeaderColumn {

    /**
     * 列号
     *
     *
     */
    int index() default 0;

    /**
     * 值
     *
     *
     */
    String value() default "";

    /**
     * 高度
     *
     *
     */
    short height() default 0;

    /**
     * 宽度
     *
     *
     */
    int width() default 0;

    /**
     * 样式
     *
     *
     */
    ATStyle[] styles() default {@ATStyle(MPHeaderColumn.DEFAULT_STYLE)};

}
