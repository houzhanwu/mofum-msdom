package com.mofum.msdom.excel.annotation.parser.impl;

import com.mofum.msdom.excel.annotation.ATMergeRange;
import com.mofum.msdom.excel.annotation.ATStyle;
import com.mofum.msdom.excel.annotation.parser.AnnotationParser;
import com.mofum.msdom.excel.metadata.MPMergeRange;
import com.mofum.msdom.excel.metadata.MPStyle;
import com.mofum.msdom.excel.utils.ReflectUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * AT 合并区域注解解析器
 *
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 13:17
 */
public class ATMergeRangeParser<T> implements AnnotationParser<T, MPMergeRange> {

    public Map<Field, MPMergeRange> parseField(T o) {

        Map<Field, MPMergeRange> map = new HashMap<Field, MPMergeRange>();

        Field[] fields = ReflectUtils.convertToClass(o).getDeclaredFields();

        for (Field field : fields) {

            if (field.isAnnotationPresent(ATMergeRange.class)) {

                ATMergeRange atMergeRange = field.getAnnotation(ATMergeRange.class);

                map.put(field, parse(atMergeRange));

            }

        }

        return map;
    }

    public Map<Type, MPMergeRange> parseType(T o) {

        Map<Type, MPMergeRange> map = new HashMap<Type, MPMergeRange>();

        Class<T> clazz = ReflectUtils.convertToClass(o);

        if (clazz.isAnnotationPresent(ATMergeRange.class)) {

            ATMergeRange atMergeRange = clazz.getAnnotation(ATMergeRange.class);

            map.put(clazz, parse(atMergeRange));

        }

        return map;
    }

    public Map<Method, MPMergeRange> parseMethod(T o) {

        Map<Method, MPMergeRange> map = new HashMap<Method, MPMergeRange>();

        Method[] methods = ReflectUtils.convertToClass(o).getDeclaredMethods();

        for (Method method : methods) {

            if (method.isAnnotationPresent(ATMergeRange.class)) {

                ATMergeRange atMergeRange = method.getAnnotation(ATMergeRange.class);

                map.put(method, parse(atMergeRange));

            }

        }

        return map;
    }

    public Map<Method, List<MPMergeRange>> parseParams(T o) {

        Map<Method, List<MPMergeRange>> map = new HashMap<Method, List<MPMergeRange>>();

        Method[] methods = ReflectUtils.convertToClass(o).getMethods();

        for (Method method : methods) {

            List<MPMergeRange> list = new ArrayList<MPMergeRange>();

            Annotation[][] parameterAnnotations = method.getParameterAnnotations();

            for (Annotation[] parameterAnnotation : parameterAnnotations) {
                for (Annotation annotation : parameterAnnotation) {
                    if (annotation instanceof ATMergeRange) {
                        ATMergeRange atMergeRange = (ATMergeRange) annotation;
                        list.add(parse(atMergeRange));
                    }
                }
            }

            map.put(method, list);
        }

        return map;
    }

    public MPMergeRange parse(ATMergeRange atMergeRange) {
        MPMergeRange mpMergeRange = new MPMergeRange();
        mpMergeRange.setStartCol(atMergeRange.startCol());
        mpMergeRange.setEndCol(atMergeRange.endCol());
        mpMergeRange.setStartRow(atMergeRange.startRow());
        mpMergeRange.setEndRow(atMergeRange.endRow());
        mpMergeRange.setValue(atMergeRange.value());
        mpMergeRange.setLevel(atMergeRange.level());
        ATStyleParser<MPStyle> parser = new ATStyleParser();

        List<MPStyle> list = new ArrayList<MPStyle>();

        for (ATStyle atStyle : atMergeRange.styles()) {
            list.add(parser.parse(atStyle));
        }
        mpMergeRange.setStyles(list.toArray(new MPStyle[]{}));
        return mpMergeRange;
    }
}
