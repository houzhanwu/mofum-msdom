package com.mofum.msdom.excel.annotation;

import com.mofum.msdom.excel.constant.ValueLevel;

import java.lang.annotation.*;

/**
 * 合并单元格区域 注解
 *
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 11:12
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ATMergeRange {

    /**
     * 合并区域
     *
     *
     */
    int[] ranges() default {};

    /**
     * 开始行
     *
     *
     */
    int startRow() default 0;

    /**
     * 截止行
     *
     *
     */
    int endRow() default 0;

    /**
     * 起始列
     *
     *
     */
    int startCol() default 0;

    /**
     * 截止列
     *
     *
     */
    int endCol() default 0;

    /**
     * 合并后的值 当LEVEL = REPLACE 的时候，该值生效
     *
     *
     */
    String value() default "";

    /**
     * 值优先级
     *
     *
     */
    ValueLevel level() default ValueLevel.NONE;

    /**
     * 样式 当LEVEL = REPLACE 的时候，该值生效
     *
     *
     */
    ATStyle[] styles() default {};
}
