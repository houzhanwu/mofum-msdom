package com.mofum.msdom.excel.annotation.template;

/**
 * Excel 模板注解
 *
 * @author yuyang@qxy37.com
 * @since 2019/1/24 0024 10:19
 */
public @interface ATExcelTemplate {

    /**
     * 模板ID
     * @return ID
     */
    String id() default "";

}
