package com.mofum.msdom.excel.annotation.parser.impl;

import com.mofum.msdom.excel.annotation.ATStyle;
import com.mofum.msdom.excel.annotation.parser.AnnotationParser;
import com.mofum.msdom.excel.metadata.MPFont;
import com.mofum.msdom.excel.metadata.MPStyle;
import com.mofum.msdom.excel.utils.ReflectUtils;
import com.mofum.msdom.excel.utils.StyleUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * AT 样式注解解析器
 *
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 13:25
 */
public class ATStyleParser<T> implements AnnotationParser<T, MPStyle> {

    public Map<Field, MPStyle> parseField(T o) {

        Map<Field, MPStyle> map = new HashMap<Field, MPStyle>();

        Field[] fields = ReflectUtils.convertToClass(o).getDeclaredFields();

        for (Field field : fields) {

            if (field.isAnnotationPresent(ATStyle.class)) {

                ATStyle atStyle = field.getAnnotation(ATStyle.class);

                map.put(field, parse(atStyle));

            }

        }

        return map;
    }

    public Map<Type, MPStyle> parseType(T o) {

        Map<Type, MPStyle> map = new HashMap<Type, MPStyle>();

        Class<T> clazz = ReflectUtils.convertToClass(o);

        if (clazz.isAnnotationPresent(ATStyle.class)) {

            ATStyle atStyle = clazz.getAnnotation(ATStyle.class);

            map.put(clazz, parse(atStyle));

        }

        return map;
    }

    public Map<Method, MPStyle> parseMethod(T o) {

        Map<Method, MPStyle> map = new HashMap<Method, MPStyle>();

        Method[] methods = ReflectUtils.convertToClass(o).getDeclaredMethods();

        for (Method method : methods) {

            if (method.isAnnotationPresent(ATStyle.class)) {

                ATStyle atStyle = method.getAnnotation(ATStyle.class);

                map.put(method, parse(atStyle));

            }

        }

        return map;
    }

    public Map<Method, List<MPStyle>> parseParams(T o) {

        Map<Method, List<MPStyle>> map = new HashMap<Method, List<MPStyle>>();

        Method[] methods = ReflectUtils.convertToClass(o).getMethods();

        for (Method method : methods) {

            List<MPStyle> list = new ArrayList<MPStyle>();

            Annotation[][] parameterAnnotations = method.getParameterAnnotations();

            for (Annotation[] parameterAnnotation : parameterAnnotations) {
                for (Annotation annotation : parameterAnnotation) {
                    if (annotation instanceof ATStyle) {
                        ATStyle atStyle = (ATStyle) annotation;
                        list.add(parse(atStyle));
                    }
                }
            }

            map.put(method, list);
        }

        return map;
    }


    public MPStyle parse(ATStyle atStyle) {
        return StyleUtils.createStyle(atStyle.value());
    }

}
