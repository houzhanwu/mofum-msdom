package com.mofum.msdom.excel.annotation;

import com.mofum.msdom.excel.converter.ExcelConverter;
import com.mofum.msdom.excel.converter.TypeConverter;
import com.mofum.msdom.excel.validator.Validator;

import java.lang.annotation.*;

/**
 * 列 注解
 *
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 11:36
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ATColumn {

    /**
     * 索引
     *
     *
     */
    int index() default 0;

    /**
     * 多个工作薄 不同索引
     *
     *
     */
    ATSheetColumnIndex[] indexes() default {};

    /**
     * 宽度
     *
     *
     */
    int width() default 0;

    /**
     * 样式
     *
     *
     */
    ATStyle[] styles() default {};

    /**
     * 转换器
     *
     *
     */
    Class<? extends TypeConverter> converter() default TypeConverter.class;

    /**
     * 解析器
     *
     *
     */
    Class<? extends ExcelConverter> excelConverter() default ExcelConverter.class;

    /**
     * 格式化
     *
     *
     */
    String format() default "";

    /**
     * 必要
     *
     *
     */
    boolean required() default false;

    /**
     * 数据校验器
     *
     *
     */
    Class<? extends Validator> validator() default Validator.class;
}
