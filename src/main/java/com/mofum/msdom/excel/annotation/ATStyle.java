package com.mofum.msdom.excel.annotation;

import org.apache.poi.ss.usermodel.Font;

import java.lang.annotation.*;

/**
 * 样式注解
 *
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 11:37
 */
@Target({ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ATStyle {
    /**
     * 样式字符串
     *
     *
     */
    String value() default "";
}
