package com.mofum.msdom.excel.annotation.parser;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

/**
 * 注解解析器
 *
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 13:08
 */
public interface AnnotationParser<Input, Output> {

    /**
     * 解析字段注解
     *
     * @param input 输入
     *
     */
    Map<Field, Output> parseField(Input input);

    /**
     * 解析类型注解
     *
     * @param input 输入
     *
     */
    Map<Type, Output> parseType(Input input);

    /**
     * 解析方法注解
     *
     * @param input 输入
     *
     */
    Map<Method, Output> parseMethod(Input input);

    /**
     * 解析参数注解
     *
     * @param input 输入
     *
     */
    Map<Method, List<Output>> parseParams(Input input);
}
