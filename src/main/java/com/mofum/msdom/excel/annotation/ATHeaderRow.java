package com.mofum.msdom.excel.annotation;

import java.lang.annotation.*;

/**
 * 头部
 *
 * @author 1615690513@qq.com
 * @since 2018/11/27 0027 14:01
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ATHeaderRow {

    /**
     * 行号
     *
     *
     */
    int index() default 0;

    /**
     * 自动大小
     *
     *
     */
    boolean autoSize() default false;

    /**
     * 列
     *
     *
     */
    ATHeaderColumn[] columns() default {};

    /**
     * 列
     *
     *
     */
    String[] values() default {};

    /**
     * 必要
     *
     *
     */
    boolean required() default true;

    /**
     * 行高
     *
     *
     */
    int height() default 0;

}
