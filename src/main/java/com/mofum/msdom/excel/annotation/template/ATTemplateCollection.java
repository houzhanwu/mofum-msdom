package com.mofum.msdom.excel.annotation.template;

/**
 * 模板集合
 *
 * @author yuyang@qxy37.com
 * @since 2019/1/24 0024 10:21
 */
public @interface ATTemplateCollection {
}
