package com.mofum.msdom.excel.annotation;

import java.lang.annotation.*;

/**
 * 工作表列索引
 *
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 11:31
 */
@Target({ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ATSheetColumnIndex {

    /**
     * 列索引
     *
     *
     */
    int index() default 0;

    /**
     * 工作表 索引
     *
     *
     */
    int value() default 0;

}
