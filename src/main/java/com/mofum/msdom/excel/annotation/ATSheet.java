package com.mofum.msdom.excel.annotation;

import com.mofum.msdom.excel.filter.DataFilter;

import java.lang.annotation.*;

/**
 * 工作表注解
 *
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 11:31
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ATSheet {

    /**
     * MPSheet 名称
     *
     *
     */
    String name() default "";

    /**
     * MPSheet 索引
     *
     *
     */
    int index() default 0;

    /**
     * 合并单元格区域
     *
     *
     */
    ATMergeRange[] ranges() default {};

    /**
     * 自动调整宽度。设置为真时，列宽度设置将失效
     *
     *
     * @see ATColumn
     */
    boolean autoSize() default false;

    /**
     * 行高
     *
     *
     */
    short rowHeight() default 0;

    /**
     * 偏移量
     *
     *
     */
    int offset() default 0;

    /**
     * 界限
     *
     *
     */
    int limit() default 0;

    /**
     * 列偏移量
     *
     *
     */
    short colOffset() default 0;

    /**
     * 列界限
     *
     *
     */
    short colLimit() default 0;

    /**
     * 数据过滤器
     *
     *
     */
    Class<? extends DataFilter>[] filters() default {};

    /**
     * 表头
     *
     *
     */
    ATHeaderRow[] headers() default {};
}
