package com.mofum.msdom.excel.annotation;

import java.lang.annotation.*;

/**
 * 工作薄 注解
 *
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 11:09
 */
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ATWorkbook {

    /**
     * 文件名
     *
     *
     */
    String name() default "";

    /**
     * 工作表
     *
     *
     */
    ATSheet[] sheets() default {};

    /**
     * 缓存数量
     *
     *
     */
    int cache() default 10;
}
