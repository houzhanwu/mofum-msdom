package com.mofum.msdom.excel.writer.impl;

import com.mofum.msdom.excel.metadata.MPMergeRange;
import com.mofum.msdom.excel.metadata.MPSheet;
import com.mofum.msdom.excel.writer.IExcelWriter;
import org.apache.poi.ss.util.CellRangeAddress;

import java.io.OutputStream;
import java.util.List;

/**
 * @author 1615690513@qq.com
 * @since 2018/11/26 0026 15:20
 */
public class BaseWriterAdapter<Data> implements IExcelWriter<Data, BaseWriterAdapter>{

    @Override
    public BaseWriterAdapter start(OutputStream output) {
        return null;
    }

    @Override
    public BaseWriterAdapter cache(int cache) {
        return null;
    }

    @Override
    public BaseWriterAdapter dataType(Class<Data> dataClass) {
        return null;
    }

    @Override
    public BaseWriterAdapter sheets(MPSheet... sheets) {
        return null;
    }

    @Override
    public BaseWriterAdapter end() {
        return null;
    }

    @Override
    public <T> BaseWriterAdapter write(List<T> datas) {
        return null;
    }

    @Override
    public <T> BaseWriterAdapter write(int sheetIndex, List<T> datas) {
        return null;
    }

    @Override
    public BaseWriterAdapter sheetIndex(int index) {
        return null;
    }

    @Override
    public BaseWriterAdapter addSheet(Class<?> dataClass, MPSheet sheet) {
        return null;
    }

    @Override
    public BaseWriterAdapter addSheets(Class<?> dataClass, MPSheet... sheets) {
        return null;
    }

    @Override
    public BaseWriterAdapter addSheets(MPSheet... sheets) {
        return null;
    }

    @Override
    public BaseWriterAdapter addSheets(Class<?>... classes) {
        return null;
    }

    @Override
    public BaseWriterAdapter addSheet(MPSheet sheet) {
        return null;
    }

    @Override
    public BaseWriterAdapter open() {
        return null;
    }

    @Override
    public BaseWriterAdapter release() {
        return null;
    }

    @Override
    public BaseWriterAdapter addSheet(Class<?> clazz) {
        return null;
    }

    @Override
    public BaseWriterAdapter addMergeRegion(MPMergeRange range) {
        return null;
    }

    @Override
    public BaseWriterAdapter addMergeRegion(CellRangeAddress range) {
        return null;
    }

    @Override
    public BaseWriterAdapter addMergeRegion(int firstRow, int lastRow, int firstCol, int lastCol) {
        return null;
    }
}
