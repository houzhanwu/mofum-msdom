package com.mofum.msdom.excel.writer.impl;

import com.mofum.msdom.excel.pager.PageCallback;
import com.mofum.msdom.excel.pager.PageContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 分页数据写入器
 *
 * @author 1615690513@qq.com
 * @since 2018/11/24 0021 15:53
 */
public class PageAnnotationExcelWriterImpl<Data, Context extends PageContext> extends AnnotationExcelWriterImpl<Data> {

    public static Logger logger = LoggerFactory.getLogger(PageAnnotationExcelWriterImpl.class);

    /**
     * 分页上下文对象
     */
    private Context context;

    /**
     * 分页回调函数
     */
    private PageCallback<Data, Context> pageCallback;

    @Override
    public PageAnnotationExcelWriterImpl end() {
        if (context == null) {
            return this;
        }
        context.setSheets(sheets);
        context.setSheetIndex(sheetIndex);
        for (int i = 0; i < context.getTotalPages(); i++) {
            context.setPageNumber(i);
            write(pageCallback.call(context));
            this.setSheetIndex(context.getSheetIndex());
            this.setSheets(context.getSheets());
        }
        //调用父类的end结束方法
        super.end();
        return this;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public PageCallback getPageCallback() {
        return pageCallback;
    }

    public void setPageCallback(PageCallback pageCallback) {
        this.pageCallback = pageCallback;
    }
}
