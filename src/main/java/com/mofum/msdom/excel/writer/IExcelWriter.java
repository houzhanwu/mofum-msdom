package com.mofum.msdom.excel.writer;

import com.mofum.msdom.excel.metadata.MPMergeRange;
import com.mofum.msdom.excel.metadata.MPSheet;
import org.apache.poi.ss.util.CellRangeAddress;

import java.io.OutputStream;
import java.util.List;

/**
 * 数据写入器
 *
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 15:53
 */
public interface IExcelWriter<Data, Writer extends IExcelWriter> {


    /**
     * 导出开始
     *
     * @param output 输出
     */
    Writer start(OutputStream output);

    /**
     * 缓存条数
     *
     * @param cache 缓存
     */
    Writer cache(int cache);

    /**
     * 数据类型
     *
     * @param dataClass 数据类型
     */
    Writer dataType(Class<Data> dataClass);

    /**
     * 工作表
     *
     * @param sheets 工作表
     */
    Writer sheets(MPSheet... sheets);

    /**
     * 结束导出
     */
    Writer end();

    /**
     * 输出数据
     *
     * @param datas 数据
     */
    <T> Writer write(List<T> datas);

    /**
     * 往指定工作薄 写入数据
     *
     * @param sheetIndex 工作薄索引
     * @param datas      数据
     */
    <T> Writer write(int sheetIndex, List<T> datas);

    /**
     * 设置当前sheet 号
     *
     * @param index
     */
    Writer sheetIndex(int index);

    /**
     * 添加工作表
     *
     * @param dataClass 数据类型
     * @param sheet     工作表
     */
    Writer addSheet(Class<?> dataClass, MPSheet sheet);

    /**
     * 添加多个工作表
     *
     * @param dataClass 数据类型
     * @param sheets    工作表
     */
    Writer addSheets(Class<?> dataClass, MPSheet... sheets);

    /**
     * 添加多个primaryClass工作表
     *
     * @param sheets 工作表
     */
    Writer addSheets(MPSheet... sheets);

    /**
     * 添加多个primaryClass工作表
     *
     * @param classes 类
     */
    Writer addSheets(Class<?>... classes);

    /**
     * 根据类型添加工作表
     *
     * @param clazz 类型
     */
    Writer addSheet(Class<?> clazz);

    /**
     * 添加工作表
     *
     * @param sheet 工作表
     */
    Writer addSheet(MPSheet sheet);

    /**
     * 打开文档流
     */
    Writer open();

    /**
     * 释放文档流
     */
    Writer release();

    /**
     * 添加合并单元格
     *
     * @return
     */
    Writer addMergeRegion(MPMergeRange range);

    /**
     * 添加合并单元格
     *
     * @return
     */
    Writer addMergeRegion(CellRangeAddress range);

    /**
     * 添加合并单元格
     *
     * @return
     */
    Writer addMergeRegion(int firstRow, int lastRow, int firstCol, int lastCol);
}
