package com.mofum.msdom.excel.exception;

/**
 * 数据验证异常
 *
 * @author 1615690513@qq.com
 * @since 2018/11/23 0023 13:44
 */
public class NumberVerificationException extends RuntimeException {

    private int number;

    public NumberVerificationException() {
        super();
    }

    public NumberVerificationException(String message) {
        super(message);
    }

    public NumberVerificationException(String message, int number) {
        super(message);
        this.number = number;
    }

    public NumberVerificationException(String message, Throwable cause) {
        super(message, cause);
    }

    public NumberVerificationException(String message, Throwable cause, int number) {
        super(message, cause);
        this.number = number;
    }

    public NumberVerificationException(Throwable cause) {
        super(cause);
    }

    protected NumberVerificationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }


    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
