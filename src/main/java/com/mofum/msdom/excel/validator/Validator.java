package com.mofum.msdom.excel.validator;

import com.mofum.msdom.excel.constant.RowContext;

/**
 * @author 1615690513@qq.com
 * @since 2018/11/24 0024 15:44
 */
public interface Validator<Context extends RowContext, ValidatorException extends Exception> {

    /**
     * 校验数据
     *
     * @param context 正文
     *
     */
    boolean doCheckData(Context context) throws ValidatorException;

}
