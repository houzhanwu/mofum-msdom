package com.mofum.msdom.excel.validator;

import com.mofum.msdom.excel.constant.RowContext;

/**
 * @author 1615690513@qq.com
 * @since 2018/11/27 0027 15:06
 */
public interface RuntimeValidator<Context extends RowContext> extends Validator<Context, RuntimeException> {
}
