package com.mofum.msdom.excel.collections.type;

import com.mofum.msdom.excel.collections.FieldExcelConvertMap;
import com.mofum.msdom.excel.collections.FieldMPColumnMap;
import com.mofum.msdom.excel.collections.IntegerStringMap;
import com.mofum.msdom.excel.metadata.MPSheet;
import org.apache.poi.ss.usermodel.Sheet;

/**
 * 工作表上下文
 *
 * @author 1615690513@qq.com
 * @since 2018/11/26 0026 12:50
 */
public class SheetContext {

    /**
     * 当前工作表使用的数据类型
     */
    private Class<?> dataClass;

    /**
     * 工作表
     */
    private MPSheet mpSheet;

    /**
     * 输出表
     */
    private Sheet sheet;

    /**
     * 当前写入行
     */
    private int currentRowIndex = -1;

    /**
     * Excel转换器
     */
    private FieldExcelConvertMap excelConvertMap;

    /**
     * 字段映射Map
     */
    private FieldMPColumnMap columnMap;

    /**
     * 字段名与Excel 行号映射关系Map
     */
    private IntegerStringMap columnIndexMap;

    public Class<?> getDataClass() {
        return dataClass;
    }

    public void setDataClass(Class<?> dataClass) {
        this.dataClass = dataClass;
    }

    public MPSheet getMpSheet() {
        return mpSheet;
    }

    public void setMpSheet(MPSheet mpSheet) {
        this.mpSheet = mpSheet;
    }

    public Sheet getSheet() {
        return sheet;
    }

    public void setSheet(Sheet sheet) {
        this.sheet = sheet;
    }

    public int getCurrentRowIndex() {
        return currentRowIndex;
    }

    public void setCurrentRowIndex(int currentRowIndex) {
        this.currentRowIndex = currentRowIndex;
    }

    public FieldExcelConvertMap getExcelConvertMap() {
        return excelConvertMap;
    }

    public void setExcelConvertMap(FieldExcelConvertMap excelConvertMap) {
        this.excelConvertMap = excelConvertMap;
    }

    public FieldMPColumnMap getColumnMap() {
        return columnMap;
    }

    public void setColumnMap(FieldMPColumnMap columnMap) {
        this.columnMap = columnMap;
    }

    public IntegerStringMap getColumnIndexMap() {
        return columnIndexMap;
    }

    public void setColumnIndexMap(IntegerStringMap columnIndexMap) {
        this.columnIndexMap = columnIndexMap;
    }

    public int getSheetIndex() {
        return mpSheet.getIndex();
    }

    public void setSheetIndex(int sheetIndex) {
        if (mpSheet == null) {
            return;
        }
        mpSheet.setIndex(sheetIndex);
    }

    public int getLimit() {
        return mpSheet.getLimit();
    }

    public int getOffset() {
        return mpSheet.getOffset();
    }

    public int getColLimit() {
        return mpSheet.getColLimit();
    }

    public int getColOffset() {
        return mpSheet.getColOffset();
    }

    public short getRowHeight() {
        return mpSheet.getRowHeight();
    }
}
