package com.mofum.msdom.excel.collections;

import com.mofum.msdom.excel.converter.ExcelConverter;

import java.lang.reflect.Field;
import java.util.HashMap;

/**
 * @author 1615690513@qq.com
 * @since 2018/11/26 0026 12:31
 */
public class FieldExcelConvertMap extends HashMap<Field, ExcelConverter> {
}
