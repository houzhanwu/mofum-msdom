package com.mofum.msdom.excel.collections;

import com.mofum.msdom.excel.metadata.MPSheet;

import java.util.HashMap;

/**
 * @author 1615690513@qq.com
 * @since 2018/11/26 0026 12:28
 */
public class MPSheetIndexMap extends HashMap<Integer, MPSheet> {
}
