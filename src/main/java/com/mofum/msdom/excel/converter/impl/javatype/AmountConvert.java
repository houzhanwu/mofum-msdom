package com.mofum.msdom.excel.converter.impl.javatype;

import com.mofum.msdom.excel.converter.TypeConverter;

import java.math.BigDecimal;

/**
 * 金额 转换器
 *
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 11:52
 */
@Deprecated
public class AmountConvert implements TypeConverter<String, BigDecimal> {

    public BigDecimal convert(String s) {
        return new BigDecimal(s).setScale(3, BigDecimal.ROUND_DOWN);
    }

}
