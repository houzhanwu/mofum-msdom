package com.mofum.msdom.excel.converter.impl.javatype;

import com.mofum.msdom.excel.converter.TypeConverter;

/**
 * 字符串转换器
 *
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 11:47
 */
public class StringConvert implements TypeConverter<String, String> {

    public String convert(String s) {
        if (s != null) {
            s = s.trim();
        }
        return s;
    }
}
