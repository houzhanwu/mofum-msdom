package com.mofum.msdom.excel.converter;

import com.mofum.msdom.excel.constant.ExcelValue;

/**
 * Excel类型转换器
 *
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 11:38
 */
public interface ExcelConverter<Source, Dest extends ExcelValue> {

    /**
     * 转换数据源成 DEST
     *
     * @param source 数据源
     *
     */
    Dest convert(Source source);

}
