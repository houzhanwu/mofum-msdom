package com.mofum.msdom.excel.converter;

/**
 * 类型转换器
 *
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 11:38
 */
public interface TypeConverter<Source, Dest> {

    /**
     * 转换数据源成 DEST
     *
     * @param source 数据源
     *
     */
    Dest convert(Source source);

}
