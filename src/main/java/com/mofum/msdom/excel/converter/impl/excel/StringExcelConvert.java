package com.mofum.msdom.excel.converter.impl.excel;

import com.mofum.msdom.excel.constant.ExcelValue;
import com.mofum.msdom.excel.converter.ExcelConverter;

/**
 * 字符串转换器
 *
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 11:47
 */
public class StringExcelConvert implements ExcelConverter<Object, ExcelValue> {

    public ExcelValue convert(Object obj) {
        String str = "";
        if (obj != null) {
            str = String.valueOf(obj);
        }
        return new ExcelValue(str);
    }
}
