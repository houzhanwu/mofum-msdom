package com.mofum.msdom.excel.metadata;

import com.mofum.msdom.excel.converter.ExcelConverter;
import com.mofum.msdom.excel.converter.TypeConverter;
import com.mofum.msdom.excel.validator.Validator;

/**
 * 列
 *
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 12:35
 */
public class MPColumn {

    /**
     * 索引
     */
    private int index;

    /**
     * 多个工作表（Sheet） 该值在write,read需要设置 differentHeads = true 才生效，将在后续版本实现此功能
     */
    private MPSheetColumnIndex[] indexes;

    /**
     * 宽度
     */
    private int width;

    /**
     * 样式
     */
    private MPStyle[] styles;

    /**
     * 转换器
     */
    private Class<? extends TypeConverter> converter;

    /**
     * Excel 转换器
     */
    private Class<? extends ExcelConverter> excelConverter;

    /**
     * 格式化
     */
    private String format;

    /**
     * 必要
     */
    private boolean required;

    /**
     * 数据校验器
     *
     *
     */
    private Class<? extends Validator> validator;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public MPSheetColumnIndex[] getIndexes() {
        return indexes;
    }

    public void setIndexes(MPSheetColumnIndex[] indexes) {
        this.indexes = indexes;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public MPStyle[] getStyles() {
        return styles;
    }

    public void setStyles(MPStyle[] styles) {
        this.styles = styles;
    }

    public Class<? extends TypeConverter> getConverter() {
        return converter;
    }

    public void setConverter(Class<? extends TypeConverter> converter) {
        this.converter = converter;
    }

    public Class<? extends ExcelConverter> getExcelConverter() {
        return excelConverter;
    }

    public void setExcelConverter(Class<? extends ExcelConverter> excelConverter) {
        this.excelConverter = excelConverter;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public Class<? extends Validator> getValidator() {
        return validator;
    }

    public void setValidator(Class<? extends Validator> validator) {
        this.validator = validator;
    }
}
