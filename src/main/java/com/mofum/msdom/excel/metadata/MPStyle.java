package com.mofum.msdom.excel.metadata;

/**
 * 样式
 *
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 12:41
 */
public class MPStyle {

    /**
     * 字体
     */
    private MPFont font;

    /**
     * 自动换行
     */
    private boolean wrapText;

    /**
     * 填充背景颜色
     */
    private String fillBackgroundColor;

    /**
     * 填充前景颜色
     */
    private String fillForegroundColor;

    /**
     * 下边框颜色
     */
    private String borderBottomColor;

    /**
     * 上边框颜色
     */
    private String borderTopColor;

    /**
     * 左边框颜色
     */
    private String borderLeftColor;

    /**
     * 右边框颜色
     */
    private String borderRightColor;

    /**
     * 隐藏
     */
    private boolean hidden;

    /**
     * 水平对齐方式
     */
    private String alignment;

    /**
     * 垂直对齐方式
     */
    private String verticalAlignment;

    /**
     * 填充格式
     */
    private String fillPattern;

    /**
     * 下边框
     */
    private String borderBottom;

    /**
     * 顶部边框
     */
    private String borderTop;

    /**
     * 左边框
     */
    private String borderLeft;

    /**
     * 右边框
     */
    private String borderRight;

    /**
     * 数据格式化
     */
    private String dataFormat;

    public MPFont getFont() {
        return font;
    }

    public void setFont(MPFont font) {
        this.font = font;
    }

    public boolean isWrapText() {
        return wrapText;
    }

    public void setWrapText(boolean wrapText) {
        this.wrapText = wrapText;
    }

    public String getFillBackgroundColor() {
        return fillBackgroundColor;
    }

    public void setFillBackgroundColor(String fillBackgroundColor) {
        this.fillBackgroundColor = fillBackgroundColor;
    }

    public String getFillForegroundColor() {
        return fillForegroundColor;
    }

    public void setFillForegroundColor(String fillForegroundColor) {
        this.fillForegroundColor = fillForegroundColor;
    }

    public String getBorderBottomColor() {
        return borderBottomColor;
    }

    public void setBorderBottomColor(String borderBottomColor) {
        this.borderBottomColor = borderBottomColor;
    }

    public String getBorderTopColor() {
        return borderTopColor;
    }

    public void setBorderTopColor(String borderTopColor) {
        this.borderTopColor = borderTopColor;
    }

    public String getBorderLeftColor() {
        return borderLeftColor;
    }

    public void setBorderLeftColor(String borderLeftColor) {
        this.borderLeftColor = borderLeftColor;
    }

    public String getBorderRightColor() {
        return borderRightColor;
    }

    public void setBorderRightColor(String borderRightColor) {
        this.borderRightColor = borderRightColor;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public String getAlignment() {
        return alignment;
    }

    public void setAlignment(String alignment) {
        this.alignment = alignment;
    }

    public String getVerticalAlignment() {
        return verticalAlignment;
    }

    public void setVerticalAlignment(String verticalAlignment) {
        this.verticalAlignment = verticalAlignment;
    }

    public String getFillPattern() {
        return fillPattern;
    }

    public void setFillPattern(String fillPattern) {
        this.fillPattern = fillPattern;
    }

    public String getBorderBottom() {
        return borderBottom;
    }

    public void setBorderBottom(String borderBottom) {
        this.borderBottom = borderBottom;
    }

    public String getBorderTop() {
        return borderTop;
    }

    public void setBorderTop(String borderTop) {
        this.borderTop = borderTop;
    }

    public String getBorderLeft() {
        return borderLeft;
    }

    public void setBorderLeft(String borderLeft) {
        this.borderLeft = borderLeft;
    }

    public String getBorderRight() {
        return borderRight;
    }

    public void setBorderRight(String borderRight) {
        this.borderRight = borderRight;
    }

    public String getDataFormat() {
        return dataFormat;
    }

    public void setDataFormat(String dataFormat) {
        this.dataFormat = dataFormat;
    }
}
