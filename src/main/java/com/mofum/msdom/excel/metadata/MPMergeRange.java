package com.mofum.msdom.excel.metadata;

import com.mofum.msdom.excel.constant.ValueLevel;

/**
 * 合并区域
 *
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 12:38
 */
public class MPMergeRange {

    /**
     * 开始行
     *
     *
     */
    private int startRow;

    /**
     * 截止行
     *
     *
     */
    private int endRow;

    /**
     * 起始列
     *
     *
     */
    private int startCol;

    /**
     * 截止列
     *
     *
     */
    private int endCol;

    /**
     * 合并后的值
     *
     *
     */
    private String value;

    /**
     * 值优先级
     *
     *
     */
    private ValueLevel level;

    /**
     * 样式
     */
    private MPStyle[] styles;


    public int getStartRow() {
        return startRow;
    }

    public void setStartRow(int startRow) {
        this.startRow = startRow;
    }

    public int getEndRow() {
        return endRow;
    }

    public void setEndRow(int endRow) {
        this.endRow = endRow;
    }

    public int getStartCol() {
        return startCol;
    }

    public void setStartCol(int startCol) {
        this.startCol = startCol;
    }

    public int getEndCol() {
        return endCol;
    }

    public void setEndCol(int endCol) {
        this.endCol = endCol;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public ValueLevel getLevel() {
        return level;
    }

    public void setLevel(ValueLevel level) {
        this.level = level;
    }

    public MPStyle[] getStyles() {
        return styles;
    }

    public void setStyles(MPStyle[] styles) {
        this.styles = styles;
    }
}
