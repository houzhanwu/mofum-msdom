package com.mofum.msdom.excel.metadata;

/**
 * 头
 *
 * @author 1615690513@qq.com
 * @since 2018/11/28 0028 14:43
 */
public class MPHeaderRow {

    /**
     * 左边行
     */
    private int index;

    /**
     * 自动大小
     */
    private boolean autoSize;

    /**
     * 列
     */
    private MPHeaderColumn[] columns;

    /**
     * 列
     */
    private String[] values;

    /**
     * 必填
     */
    private boolean required;

    /**
     * 行高
     */
    private int height;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isAutoSize() {
        return autoSize;
    }

    public void setAutoSize(boolean autoSize) {
        this.autoSize = autoSize;
    }

    public MPHeaderColumn[] getColumns() {
        return columns;
    }

    public void setColumns(MPHeaderColumn[] columns) {
        this.columns = columns;
    }

    public String[] getValues() {
        return values;
    }

    public void setValues(String[] values) {
        this.values = values;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
