package com.mofum.msdom.excel.metadata;

import com.mofum.msdom.excel.annotation.ATColumn;
import com.mofum.msdom.excel.filter.DataFilter;

/**
 * 工作表
 *
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 12:35
 */
public class MPSheet {

    public static final MPSheet DEFAUT_SHEET = MPSheet.createDefault();

    /**
     * MPSheet 名称
     */
    private String name;

    /**
     * MPSheet 索引
     */
    private int index;

    /**
     * 合并单元格区域
     */
    private MPMergeRange[] ranges;

    /**
     * 自动调整宽度。设置为真时，列宽度设置将失效
     *
     * @see ATColumn
     */
    private boolean autoSize;

    /**
     * 行高
     */
    private short rowHeight;

    /**
     * 偏移量
     */
    private int offset;

    /**
     * 界限
     */
    private int limit;

    /**
     * 列偏移量
     *
     *
     */
    private short colOffset;

    /**
     * 列界限
     *
     *
     */
    private short colLimit;

    /**
     * 数据过滤器
     *
     *
     */
    private Class<? extends DataFilter>[] filters;

    /**
     * 头部
     */
    private MPHeaderRow[] headerRows;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public MPMergeRange[] getRanges() {
        return ranges;
    }

    public void setRanges(MPMergeRange[] ranges) {
        this.ranges = ranges;
    }

    public boolean isAutoSize() {
        return autoSize;
    }

    public void setAutoSize(boolean autoSize) {
        this.autoSize = autoSize;
    }

    public short getRowHeight() {
        return rowHeight;
    }

    public void setRowHeight(short rowHeight) {
        this.rowHeight = rowHeight;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public short getColOffset() {
        return colOffset;
    }

    public void setColOffset(short colOffset) {
        this.colOffset = colOffset;
    }

    public short getColLimit() {
        return colLimit;
    }

    public void setColLimit(short colLimit) {
        this.colLimit = colLimit;
    }

    public Class<? extends DataFilter>[] getFilters() {
        return filters;
    }

    public void setFilters(Class<? extends DataFilter>[] filters) {
        this.filters = filters;
    }

    public MPHeaderRow[] getHeaderRows() {
        return headerRows;
    }

    public void setHeaderRows(MPHeaderRow[] headerRows) {
        this.headerRows = headerRows;
    }

    private static MPSheet createDefault() {
        MPSheet mpSheet = new MPSheet();
        mpSheet.setAutoSize(false);
        mpSheet.setColLimit((short) 0);
        mpSheet.setColOffset((short) 0);
        mpSheet.setLimit(0);
        mpSheet.setOffset(0);
        mpSheet.setIndex(0);
        mpSheet.setName("Sheet 1");
        return mpSheet;
    }
}
