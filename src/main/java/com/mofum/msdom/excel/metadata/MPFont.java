package com.mofum.msdom.excel.metadata;

/**
 * @author 1615690513@qq.com
 * @since 2018/11/30 0030 15:39
 */
public class MPFont {

    /**
     * 字体名称
     */
    private String fontName;

    /**
     * 倾斜
     */
    private boolean italic;

    /**
     * 颜色
     */
    private String color;

    /**
     * 字体高度
     */
    private short fontHeight;

    /**
     * 删除
     */
    private boolean strikeout;

    /**
     * 下划线
     */
    private String underline;

    /**
     * 加粗
     */
    private boolean bold;

    public String getFontName() {
        return fontName;
    }

    public void setFontName(String fontName) {
        this.fontName = fontName;
    }

    public boolean isItalic() {
        return italic;
    }

    public void setItalic(boolean italic) {
        this.italic = italic;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public short getFontHeight() {
        return fontHeight;
    }

    public void setFontHeight(short fontHeight) {
        this.fontHeight = fontHeight;
    }

    public boolean isStrikeout() {
        return strikeout;
    }

    public void setStrikeout(boolean strikeout) {
        this.strikeout = strikeout;
    }

    public String getUnderline() {
        return underline;
    }

    public void setUnderline(String underline) {
        this.underline = underline;
    }

    public boolean isBold() {
        return bold;
    }

    public void setBold(boolean bold) {
        this.bold = bold;
    }
}
