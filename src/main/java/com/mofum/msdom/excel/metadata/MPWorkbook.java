package com.mofum.msdom.excel.metadata;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 工作薄
 *
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 12:36
 */
public class MPWorkbook {

    public static final MPWorkbook DEFAULT_WORKBOOK = MPWorkbook.createWorkbook();

    /**
     * 默认十条缓存
     */
    public static final int DEFAULT_CACHE = 10;

    /**
     * 文件名
     */
    private String name;

    /**
     * 工作表
     */
    private MPSheet[] sheets;

    /**
     * 缓存数量
     */
    private int cache = DEFAULT_CACHE;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MPSheet[] getSheets() {
        return sheets;
    }

    public void setSheets(MPSheet[] sheets) {
        this.sheets = sheets;
    }

    public int getCache() {
        return cache;
    }

    public void setCache(int cache) {
        this.cache = cache;
    }

    private static MPWorkbook createWorkbook() {

        MPWorkbook mpWorkbook = new MPWorkbook();

        mpWorkbook.setCache(DEFAULT_CACHE);

        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmssS");

        mpWorkbook.setName("workbook_" + format.format(new Date()));

        return mpWorkbook;
    }
}
