package com.mofum.msdom.excel.metadata;

/**
 * 头列
 *
 * @author 1615690513@qq.com
 * @since 2018/11/28 0028 14:44
 */
public class MPHeaderColumn {

    public static final String DEFAULT_STYLE = "alignment:center;vertical-alignment:center;font-bold:true";

    /**
     * 列号
     */
    private int index;

    /**
     * 值
     */
    private String value;

    /**
     * 高度
     */
    private short height;

    /**
     * 宽度
     */
    private int width;

    /**
     * 样式
     */
    private MPStyle[] styles;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public short getHeight() {
        return height;
    }

    public void setHeight(short height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public MPStyle[] getStyles() {
        return styles;
    }

    public void setStyles(MPStyle[] styles) {
        this.styles = styles;
    }
}
