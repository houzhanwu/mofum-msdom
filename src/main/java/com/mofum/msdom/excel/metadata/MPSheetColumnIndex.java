package com.mofum.msdom.excel.metadata;

/**
 * 工作表列值
 *
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 12:35
 */
public class MPSheetColumnIndex {

    /**
     * 列索引
     */
    private int index;

    /**
     * 工作表 索引
     */
    private int value;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
