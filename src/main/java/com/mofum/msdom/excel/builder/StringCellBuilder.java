package com.mofum.msdom.excel.builder;

import com.mofum.msdom.excel.metadata.MPStyle;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.ss.usermodel.*;

import java.util.Calendar;
import java.util.Date;

/**
 * 字符串格子构造器
 *
 * @author 1615690513@qq.com
 * @since 2018/11/20 0020 17:55
 */
public class StringCellBuilder {

    /**
     * 构造格子
     */
    private Cell cell;

    /**
     * 行
     */
    private Row row;

    public StringCellBuilder(Row row) {
        this.row = row;
    }

    /**
     * 初始化 格子
     *
     * @param cellIndex 格子索引
     */
    public StringCellBuilder newCell(int cellIndex) {
        cell = row.createCell(cellIndex);
        return this;
    }

    /**
     * 构建格子
     */
    public Cell build() {
        return cell;
    }

    /**
     * 添加注释
     *
     * @param value   注释内容
     * @param drawing 画图
     * @param anchor  锚
     * @param author  作者
     */
    public StringCellBuilder comment(RichTextString value, Drawing drawing, ClientAnchor anchor, String author) {
        Comment comment = cell.getCellComment();

        if (comment == null) {
            comment = drawing.createCellComment(anchor);
            cell.setCellComment(comment);
        }
        comment.setString(value);
        comment.setAuthor(author);
        return this;
    }

    /**
     * 设置格子值
     *
     * @param val 格子值
     */
    public StringCellBuilder value(String val) {
        cell.setCellValue(val);
        return this;
    }

    /**
     * 设置格子值
     *
     * @param val 格子值
     */
    public StringCellBuilder value(Object val) {

        if (val instanceof String) {
            value((String) val);
        }
        if (val instanceof Boolean) {
            cell.setCellValue((Boolean) val);
        }
        if (val instanceof Long) {
            cell.setCellValue((Long) val);
        }
        if (val instanceof Short) {
            cell.setCellValue((Short) val);
        }
        if (val instanceof Integer) {
            cell.setCellValue((Integer) val);
        }
        if (val instanceof Byte) {
            cell.setCellValue((Byte) val);
        }
        if (val instanceof Float) {
            cell.setCellValue((Float) val);
        }
        if (val instanceof Double) {
            cell.setCellValue((Double) val);
        }
        if (val instanceof Character) {
            cell.setCellValue((Character) val);
        }
        if (val instanceof Date) {
            cell.setCellValue((Date) val);
        }
        if (val instanceof RichTextString) {
            cell.setCellValue((RichTextString) val);
        }
        if (val instanceof Calendar) {
            cell.setCellValue((Calendar) val);
        }

        if(val != null){

            if(int.class.equals(val.getClass())){
                cell.setCellValue((int) val);
            }
            if(short.class.equals(val.getClass())){
                cell.setCellValue((short) val);
            }
            if(byte.class.equals(val.getClass())){
                cell.setCellValue((byte) val);
            }
            if(char.class.equals(val.getClass())){
                cell.setCellValue((char) val);
            }
            if(long.class.equals(val.getClass())){
                cell.setCellValue((long) val);
            }
            if(double.class.equals(val.getClass())){
                cell.setCellValue((double) val);
            }
            if(float.class.equals(val.getClass())){
                cell.setCellValue((float) val);
            }
            if(boolean.class.equals(val.getClass())){
                cell.setCellValue((boolean) val);
            }

        }

        return this;
    }

    /**
     * 设置格子值
     *
     * @param val 格子值
     */
    public StringCellBuilder value(boolean val) {
        cell.setCellValue(val);
        return this;
    }

    /**
     * 设置格子值
     *
     * @param val 格子值
     */
    public StringCellBuilder value(double val) {
        cell.setCellValue(val);
        return this;
    }

    /**
     * 设置格子值
     *
     * @param val 格子值
     */
    public StringCellBuilder value(int val) {
        cell.setCellValue(val);
        return this;
    }

    /**
     * 设置格子值
     *
     * @param val 格子值
     */
    public StringCellBuilder value(short val) {
        cell.setCellValue(val);
        return this;
    }

    /**
     * 设置格子值
     *
     * @param val 格子值
     */
    public StringCellBuilder value(long val) {
        cell.setCellValue(val);
        return this;
    }

    /**
     * 设置格子值
     *
     * @param val 格子值
     */
    public StringCellBuilder value(byte val) {
        cell.setCellValue(val);
        return this;
    }

    /**
     * 设置格子值
     *
     * @param val 格子值
     */
    public StringCellBuilder value(float val) {
        cell.setCellValue(val);
        return this;
    }

    /**
     * 设置格子值
     *
     * @param val 格子值
     */
    public StringCellBuilder value(char val) {
        cell.setCellValue(val);
        return this;
    }

    /**
     * 设置格子值
     *
     * @param val 格子值
     */
    public StringCellBuilder value(Date val) {
        cell.setCellValue(val);
        return this;
    }

    /**
     * 设置格子值
     *
     * @param val 格子值
     */
    public StringCellBuilder value(RichTextString val) {
        cell.setCellValue(val);
        return this;
    }

    /**
     * 设置格子值
     *
     * @param val 格子值
     */
    public StringCellBuilder value(Calendar val) {
        cell.setCellValue(val);
        return this;
    }

    /**
     * 设置格子类型
     *
     * @param type 格子类型
     */
    public StringCellBuilder type(CellType type) {
        cell.setCellType(type);
        return this;
    }

    /**
     * 设置格子超链接
     *
     * @param link 超链接
     */
    public StringCellBuilder hyperlink(Hyperlink link) {
        cell.setHyperlink(link);
        return this;
    }

    /**
     * 设置格子公式
     *
     * @param formula 公式
     */
    public StringCellBuilder formula(String formula) {
        cell.setCellFormula(formula);
        return this;
    }

    /**
     * 设置格子ERROR 格式
     *
     * @param error
     */
    public StringCellBuilder error(byte error) {
        cell.setCellErrorValue(error);
        return this;
    }

    /**
     * 作为活动单元格
     */
    public StringCellBuilder asActiveCell() {
        cell.setAsActiveCell();
        return this;
    }

    /**
     * 设置样式
     *
     * @param mpStyle
     */
    public StringCellBuilder style(MPStyle mpStyle) {
        Workbook workbook = row.getSheet().getWorkbook();
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        style.setFont(font);
        cell.setCellStyle(style);
        return this;
    }

    /**
     * 设置样式
     *
     * @param style
     */
    public StringCellBuilder style(CellStyle style) {
        cell.setCellStyle(style);
        return this;
    }

    public Row getRow() {
        return row;
    }

    public void setRow(Row row) {
        this.row = row;
    }
}
