package com.mofum.msdom.excel.builder;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

/**
 * 行构造器
 *
 * @author 1615690513@qq.com
 * @since 2018/11/20 0020 19:35
 */
public class RowBuilder {

    /**
     * 行
     */
    private Row row;

    /**
     * 表格
     */
    private Sheet sheet;

    public RowBuilder(Sheet sheet) {
        this.sheet = sheet;
    }

    /**
     * 初始化 行
     *
     * @param rowIndex 行索引
     *
     */
    public RowBuilder newRow(int rowIndex) {
        row = sheet.createRow(rowIndex);
        return this;
    }

    public Row build() {
        return row;
    }

    /**
     * 行号
     *
     * @param rowIndex 行索引
     *
     */
    public RowBuilder num(int rowIndex) {
        row.setRowNum(rowIndex);
        return this;
    }

    /**
     * 行样式
     *
     * @param cellStyle 格子样式
     *
     */
    public RowBuilder style(CellStyle cellStyle) {
        row.setRowStyle(cellStyle);
        return this;
    }


    public RowBuilder zeroHeight(boolean zeroHeightFlag) {
        row.setZeroHeight(zeroHeightFlag);
        return this;
    }

    /**
     * 行高度
     *
     * @param height 高度
     *
     */
    public RowBuilder height(short height) {
        if (height > 0) {
            row.setHeight(height);
        }
        return this;
    }

    public Sheet getSheet() {
        return sheet;
    }

    public void setSheet(Sheet sheet) {
        this.sheet = sheet;
    }
}
