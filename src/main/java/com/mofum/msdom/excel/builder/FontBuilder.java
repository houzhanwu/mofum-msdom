package com.mofum.msdom.excel.builder;

import com.mofum.msdom.excel.metadata.MPFont;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.FontUnderline;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

/**
 * @author 1615690513@qq.com
 * @since 2018/11/30 0030 11:45
 */
public class FontBuilder {

    /**
     * 工作簿
     */
    private Workbook workbook = null;

    /**
     * 样式
     */
    private Font font = null;


    public FontBuilder(Workbook workbook) {
        this.workbook = workbook;
    }

    public Font build() {
        return font;
    }

    /**
     * 初始化 字体
     *
     *
     */
    public FontBuilder newFont() {
        font = workbook.createFont();
        return this;
    }

    public FontBuilder fontName(String fontName) {
        font.setFontName(fontName);
        return this;
    }

    public FontBuilder italic(boolean flag) {
        font.setItalic(flag);
        return this;
    }

    public FontBuilder color(String color) {
        if (color == null) {
            return this;
        }
        IndexedColors colors = IndexedColors.valueOf(color.toUpperCase());
        font.setColor(colors.index);
        return this;
    }

    public FontBuilder color(IndexedColors color) {
        font.setColor(color.index);
        return this;
    }

    public FontBuilder fontHeight(short fontHeight) {
        font.setFontHeight(fontHeight);
        return this;
    }

    public FontBuilder strikeout(boolean flag) {
        font.setStrikeout(flag);
        return this;
    }

    public FontBuilder underline(int value) {
        font.setUnderline(FontUnderline.valueOf(value).getByteValue());
        return this;
    }

    public FontBuilder underline(String value) {
        if (value == null) {
            return this;
        }
        font.setUnderline(FontUnderline.valueOf(value.toUpperCase()).getByteValue());
        return this;
    }

    public FontBuilder bold(boolean flag) {
        font.setBold(flag);
        return this;
    }

    public FontBuilder mpFont(MPFont mpFont) {
        if (mpFont == null) {
            return this;
        }

        newFont();

        if (mpFont.getFontHeight() > 0) {
            fontHeight(mpFont.getFontHeight());
        }
        if (mpFont.getColor() != null) {
            color(mpFont.getColor());
        }

        if (mpFont.getFontName() != null) {
            fontName(mpFont.getFontName());
        }

        italic(mpFont.isItalic());
        strikeout(mpFont.isStrikeout());

        if (mpFont.getUnderline() != null) {
            underline(mpFont.getUnderline());
        }

        bold(mpFont.isBold());
        return this;
    }
}
