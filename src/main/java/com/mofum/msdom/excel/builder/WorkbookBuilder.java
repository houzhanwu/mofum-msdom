package com.mofum.msdom.excel.builder;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

/**
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 15:48
 */
public class WorkbookBuilder {

    /**
     * 工作薄
     */
    private Workbook workbook;

    /**
     * 初始化 工作薄
     *
     * @param cache 缓存数量
     *
     */
    public WorkbookBuilder newWorkbook(int cache) {
        workbook = new SXSSFWorkbook(cache);
        return this;
    }

    public Workbook build() {
        return workbook;
    }

}
