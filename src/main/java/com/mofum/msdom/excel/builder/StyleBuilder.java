package com.mofum.msdom.excel.builder;

import com.mofum.msdom.excel.metadata.MPStyle;
import org.apache.poi.ss.usermodel.*;

/**
 * @author 1615690513@qq.com
 * @since 2018/11/30 0030 10:36
 */
public class StyleBuilder {

    /**
     * 工作簿
     */
    private Workbook workbook = null;

    /**
     * 样式
     */
    private CellStyle style = null;


    public StyleBuilder(Workbook workbook) {
        this.workbook = workbook;
    }

    public CellStyle build() {
        return style;
    }

    /**
     * 初始化 样式
     */
    public StyleBuilder newStyle() {
        style = workbook.createCellStyle();
        return this;
    }

    /**
     * 自动换行
     */
    public StyleBuilder wrapText(boolean flag) {
        style.setWrapText(flag);
        return this;
    }

    public StyleBuilder fillBackgroundColor(String color) {
        if (color == null) {
            return this;
        }
        IndexedColors colors = IndexedColors.valueOf(color.toUpperCase());
        style.setFillBackgroundColor(colors.index);
        return this;
    }


    public StyleBuilder dataFormat(String format) {
        DataFormat dataFormat = workbook.createDataFormat();
        style.setDataFormat(dataFormat.getFormat(format));
        return this;
    }

    /**
     * 填充背景颜色
     *
     * @param color
     */
    public StyleBuilder fillBackgroundColor(IndexedColors color) {
        style.setFillBackgroundColor(color.index);
        return this;
    }

    public StyleBuilder fillForegroundColor(String color) {
        if (color == null) {
            return this;
        }
        IndexedColors colors = IndexedColors.valueOf(color.toUpperCase());
        style.setFillForegroundColor(colors.index);
        return this;
    }

    /**
     * 填充前景颜色
     *
     * @param color
     */
    public StyleBuilder fillForegroundColor(IndexedColors color) {
        style.setFillForegroundColor(color.index);
        return this;
    }

    public StyleBuilder bottomBorderColor(String color) {
        if (color == null) {
            return this;
        }
        IndexedColors colors = IndexedColors.valueOf(color.toUpperCase());
        style.setBottomBorderColor(colors.index);
        return this;
    }

    /**
     * 底线颜色
     *
     * @param color
     */
    public StyleBuilder bottomBorderColor(IndexedColors color) {
        style.setBottomBorderColor(color.index);
        return this;
    }

    public StyleBuilder topBorderColor(String color) {
        if (color == null) {
            return this;
        }
        IndexedColors colors = IndexedColors.valueOf(color.toUpperCase());
        style.setTopBorderColor(colors.index);
        return this;
    }

    /**
     * 顶线颜色
     *
     * @param color
     */
    public StyleBuilder topBorderColor(IndexedColors color) {
        style.setTopBorderColor(color.index);
        return this;
    }

    public StyleBuilder leftBorderColor(String color) {
        if (color == null) {
            return this;
        }
        IndexedColors colors = IndexedColors.valueOf(color.toUpperCase());
        style.setLeftBorderColor(colors.index);
        return this;
    }

    /**
     * 左线颜色
     *
     * @param color
     */
    public StyleBuilder leftBorderColor(IndexedColors color) {
        style.setLeftBorderColor(color.index);
        return this;
    }

    public StyleBuilder rightBorderColor(String color) {
        if (color == null) {
            return this;
        }
        IndexedColors colors = IndexedColors.valueOf(color.toUpperCase());
        style.setRightBorderColor(colors.index);
        return this;
    }

    /**
     * 右线颜色
     *
     * @param color
     */
    public StyleBuilder rightBorderColor(IndexedColors color) {
        style.setRightBorderColor(color.index);
        return this;
    }

    public StyleBuilder fillPattern(String fill) {
        if (fill == null) {
            return this;
        }
        style.setFillPattern(FillPatternType.valueOf(fill.toUpperCase()));
        return this;
    }

    public StyleBuilder borderBottom(String name) {
        if (name == null) {
            return this;
        }
        style.setBorderBottom(BorderStyle.valueOf(name.toUpperCase()));
        return this;
    }

    public StyleBuilder borderTop(String name) {
        if (name == null) {
            return this;
        }
        style.setBorderTop(BorderStyle.valueOf(name.toUpperCase()));
        return this;
    }

    public StyleBuilder borderRight(String name) {
        if (name == null) {
            return this;
        }
        style.setBorderRight(BorderStyle.valueOf(name.toUpperCase()));
        return this;
    }

    public StyleBuilder borderLeft(String name) {
        if (name == null) {
            return this;
        }
        style.setBorderLeft(BorderStyle.valueOf(name.toUpperCase()));
        return this;
    }

    public StyleBuilder hidden(boolean flag) {
        style.setHidden(flag);
        return this;
    }

    public StyleBuilder font(Font font) {
        style.setFont(font);
        return this;
    }

    public StyleBuilder alignment(String name) {
        if (name == null) {
            return this;
        }
        HorizontalAlignment alignment = HorizontalAlignment.valueOf(name.toUpperCase());
        style.setAlignment(alignment);
        return this;
    }

    public StyleBuilder verticalAlignment(String name) {
        if (name == null) {
            return this;
        }
        VerticalAlignment alignment = VerticalAlignment.valueOf(name.toUpperCase());
        style.setVerticalAlignment(alignment);
        return this;
    }

    public StyleBuilder mpStyle(MPStyle mpStyle) {
        if (mpStyle == null) {
            return this;
        }

        newStyle();

        FontBuilder fontBuilder = new FontBuilder(workbook);

        Font font = fontBuilder.mpFont(mpStyle.getFont()).build();

        if (mpStyle.getAlignment() != null) {
            alignment(mpStyle.getAlignment());
        }

        if (mpStyle.getVerticalAlignment() != null) {
            verticalAlignment(mpStyle.getVerticalAlignment());
        }

        if (font != null) {
            font(font);
        }


        hidden(mpStyle.isHidden());
        wrapText(mpStyle.isWrapText());

        if (mpStyle.getFillBackgroundColor() != null) {
            fillBackgroundColor(mpStyle.getFillBackgroundColor());
        }

        if (mpStyle.getFillForegroundColor() != null) {
            fillForegroundColor(mpStyle.getFillForegroundColor());
        }

        if (mpStyle.getBorderLeftColor() != null) {
            leftBorderColor(mpStyle.getBorderLeftColor());
        }

        if (mpStyle.getBorderRightColor() != null) {
            rightBorderColor(mpStyle.getBorderRightColor());
        }

        if (mpStyle.getBorderTopColor() != null) {
            topBorderColor(mpStyle.getBorderTopColor());
        }

        if (mpStyle.getBorderBottomColor() != null) {
            bottomBorderColor(mpStyle.getBorderBottomColor());
        }

        if (mpStyle.getFillPattern() != null) {
            fillPattern(mpStyle.getFillPattern());
        }

        if (mpStyle.getBorderBottom() != null) {
            borderBottom(mpStyle.getBorderBottom());
        }

        if (mpStyle.getBorderTop() != null) {
            borderTop(mpStyle.getBorderTop());
        }

        if (mpStyle.getBorderLeft() != null) {
            borderLeft(mpStyle.getBorderLeft());
        }

        if (mpStyle.getBorderRight() != null) {
            borderRight(mpStyle.getBorderRight());
        }

        if (mpStyle.getDataFormat() != null) {
            dataFormat(mpStyle.getDataFormat());
        }
        return this;
    }
}
