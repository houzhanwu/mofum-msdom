package com.mofum.msdom.excel.builder;

import com.mofum.msdom.excel.metadata.MPMergeRange;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellAddress;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFSheet;

/**
 * 工作表 构造器
 *
 * @author 1615690513@qq.com
 * @since 2018/11/20 0020 19:47
 */
public class SheetBuilder {

    /**
     * 工作簿
     */
    private Workbook workbook = null;

    /**
     * 工作表
     */
    private Sheet sheet = null;

    public SheetBuilder(Workbook workbook) {
        this.workbook = workbook;
    }

    public Sheet build() {
        return sheet;
    }

    /**
     * 初始化 行
     *
     * @param name 行索引
     *
     */
    public SheetBuilder newSheet(String name) {
        sheet = workbook.createSheet(name);
        return this;
    }

    /**
     * 活动单元格
     *
     * @param address 单元格地址
     *
     */
    public SheetBuilder activeCell(CellAddress address) {
        sheet.setActiveCell(address);
        return this;
    }

    /**
     * 为一个范围添加公式
     *
     * @param formula 公式
     * @param address 范围地址
     *
     */
    public SheetBuilder arrayFormula(String formula, CellRangeAddress address) {
        sheet.setArrayFormula(formula, address);
        return this;
    }

    /**
     * 自动分页
     *
     * @param flag 标志
     *
     */
    public SheetBuilder autoBreaks(boolean flag) {
        sheet.setAutobreaks(flag);
        return this;
    }

    /**
     * 列分页
     *
     * @param value 值
     *
     */
    public SheetBuilder columnBreak(int value) {
        sheet.setColumnBreak(value);
        return this;
    }

    /**
     * 行分页
     *
     * @param value 值
     *
     */
    public SheetBuilder rowBreak(int value) {
        sheet.setRowBreak(value);
        return this;
    }

    /**
     * 隐藏列
     *
     * @param colIndex 列值
     * @param flag     标志
     *
     */
    public SheetBuilder columnHidden(int colIndex, boolean flag) {
        sheet.setColumnHidden(colIndex, flag);
        return this;
    }

    /**
     * 自动计算列大小
     *
     * @param column 列
     *
     */
    public SheetBuilder trackColumnForAutoSizing(int column) {
        if (sheet instanceof SXSSFSheet) {
            ((SXSSFSheet) sheet).trackColumnForAutoSizing(column);
        }
        sheet.autoSizeColumn(column, true);
        return this;
    }

    /**
     * 合并范围区域内单元格
     *
     * @param addresses 范围地址
     *
     */
    public SheetBuilder mergedRegion(CellRangeAddress addresses) {
        sheet.addMergedRegion(addresses);
        return this;
    }

    /**
     * 合并范围区域内单元格
     *
     * @param columnIndex 列索引
     * @param width       宽度
     *
     */
    public SheetBuilder columnWidth(int columnIndex, int width) {
        sheet.setColumnWidth(columnIndex, width);
        return this;
    }

    /**
     * 合并范围区域内单元格
     *
     * @param range 范围
     *
     */
    public SheetBuilder mergedRegion(MPMergeRange range) {
        sheet.addMergedRegion(new CellRangeAddress(range.getStartRow(), range.getEndRow(), range.getStartCol(), range.getEndCol()));
        return this;
    }

}
