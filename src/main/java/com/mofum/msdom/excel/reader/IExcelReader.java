package com.mofum.msdom.excel.reader;

import com.mofum.msdom.excel.metadata.MPSheet;
import com.mofum.msdom.excel.parser.ExcelParserCallback;

import java.io.File;
import java.io.InputStream;

/**
 * Excel 读取器
 *
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 15:53
 */
public interface IExcelReader<Data, Callback extends ExcelParserCallback<Data>> {

    /**
     * 读取二进制流
     *
     * @param inputStream 二进制流
     * @param dataClass   数据类型
     * @param callback    数据解析完成的回调方法
     */
    void read(InputStream inputStream, Class<Data> dataClass, Callback callback);

    /**
     * 读取数据
     *
     * @param input     输入文件
     * @param dataClass 数据类型
     * @param callback  数据解析完成的回调方法
     */
    void read(File input, Class<Data> dataClass, Callback callback);

    /**
     * 读取数据
     *
     * @param input     输入文件
     * @param dataClass 数据类型
     * @param callback  数据解析完成的回调方法
     * @param limit     界限
     * @param offset    根据界限偏移
     */
    void read(File input, Class<Data> dataClass, Callback callback, int limit, int offset);

    /**
     * 读取数据
     *
     * @param input     输入文件
     * @param dataClass 数据类型
     * @param callback  数据解析完成的回调方法
     * @param sheets    sheet相关的信息
     */
    void read(File input, Class<Data> dataClass, Callback callback, MPSheet... sheets);

    /**
     * 读取数据
     *
     * @param input     输入文件
     * @param dataClass 数据类型
     * @param callback  数据解析完成的回调方法
     * @param sheets    sheet相关的信息
     */
    void read(File input, Class<Data> dataClass, Callback callback, Integer... sheets);

    /**
     * 读取数据
     *
     * @param input        输入文件
     * @param dataClass    数据类型
     * @param callback     数据解析完成的回调方法
     * @param sheetForeach 是否遍历解析sheet
     */
    void read(File input, Class<Data> dataClass, Callback callback, boolean sheetForeach);

    /**
     * 读取数据
     *
     * @param input        输入文件
     * @param dataClass    数据类型
     * @param callback     数据解析完成的回调方法
     * @param limit        界限
     * @param offset       根据界限的偏移数量
     * @param sheets       sheet 配置信息
     * @param sheetForeach 是否便利读取sheet
     */
    void read(File input, Class<Data> dataClass, Callback callback, int limit, int offset, MPSheet[] sheets, boolean sheetForeach);

}
