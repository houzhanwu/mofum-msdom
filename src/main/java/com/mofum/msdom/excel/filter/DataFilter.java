package com.mofum.msdom.excel.filter;

import com.mofum.msdom.excel.constant.RowContext;

/**
 * 数据过滤器
 *
 * @author 1615690513@qq.com
 * @since 2018/11/23 0023 23:56
 */
public interface DataFilter<Context extends RowContext> {

    /**
     * 执行过滤器
     *
     * @param context
     */
    boolean execute(Context context);

}
