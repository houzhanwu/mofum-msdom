package com.mofum.msdom.excel.constant;

import org.apache.poi.ss.usermodel.CellType;

/**
 * Excel值
 *
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 11:12
 */
public class ExcelValue {

    /**
     * 值
     */
    private Object value;

    /**
     * 类型
     */
    private CellType type = CellType.STRING;

    /**
     * 数据格式化
     */
    private String dataFormat;

    public ExcelValue() {
    }

    public ExcelValue(String value) {
        this.value = value;
    }

    public ExcelValue(String value, CellType type) {
        this.value = value;
        this.type = type;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public CellType getType() {
        return type;
    }

    public void setType(CellType type) {
        this.type = type;
    }

    public String getDataFormat() {
        return dataFormat;
    }

    public void setDataFormat(String dataFormat) {
        this.dataFormat = dataFormat;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
