package com.mofum.msdom.excel.constant;

/**
 * 行上下文
 *
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 11:12
 */
public class RowContext<T> {

    /**
     * 工作表索引
     */
    private int sheetIndex;

    /**
     * 行索引
     */
    private int rowIndex;

    /**
     * 数据
     */
    private T data;

    public int getSheetIndex() {
        return sheetIndex;
    }

    public void setSheetIndex(int sheetIndex) {
        this.sheetIndex = sheetIndex;
    }

    public int getRowIndex() {
        return rowIndex;
    }

    public void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
