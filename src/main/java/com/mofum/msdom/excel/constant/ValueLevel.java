package com.mofum.msdom.excel.constant;

/**
 * 值优先等级
 *
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 11:12
 */
public enum ValueLevel {

    NONE(-1), //没有
    NORMAL(1), //正常
    REPLACE(2);//替换级别

    /**
     * LEVEL 级别
     */
    private int level;


    public static ValueLevel valueOf(int level) {
        for (ValueLevel valueLevel : ValueLevel.values()) {

            if (level == valueLevel.getLevel()) {
                return valueLevel;
            }
        }
        return null;
    }

    ValueLevel(int level) {
        this.level = level;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
