package com.mofum.msdom.excel.demo;

import com.mofum.msdom.excel.demo.callback.TestModelCallback;
import com.mofum.msdom.excel.demo.model.TestModel;
import com.mofum.msdom.excel.reader.IExcelReader;
import com.mofum.msdom.excel.reader.impl.AnnotationExcelReaderImpl;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 19:47
 */
public class ReadDemoByInputStream {

    public static void main(String[] args) throws IOException {

        //创建Reader ，对Test模型进行解析
        IExcelReader<TestModel, TestModelCallback> reader = new AnnotationExcelReaderImpl<TestModel, TestModelCallback>();

        // 输入流

        long time = System.currentTimeMillis();

        BufferedInputStream fileInputStream = new BufferedInputStream(new FileInputStream(new File("sxssf1.xlsx")));

        //POI 读取Excel 文件
        reader.read(fileInputStream, TestModel.class, new TestModelCallback());

        System.out.println("解析耗时：" + String.valueOf((System.currentTimeMillis() - time) / 1000) + "s");
    }

}
