package com.mofum.msdom.excel.demo.callback;

import com.mofum.msdom.excel.constant.RowContext;
import com.mofum.msdom.excel.demo.model.TestModel;
import com.mofum.msdom.excel.parser.ExcelParserCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 19:49
 */
public class TestModelCallback implements ExcelParserCallback<TestModel> {

    public static Logger logger = LoggerFactory.getLogger(TestModelCallback.class);

    @Override
    public void parseDataAfter(RowContext<TestModel> data) {
        logger.info(data.getData().toString());
    }
}
