package com.mofum.msdom.excel.demo;

import com.mofum.msdom.excel.demo.model.TestModel;
import com.mofum.msdom.excel.writer.IExcelWriter;
import com.mofum.msdom.excel.writer.impl.AnnotationExcelWriterImpl;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class WriteDemo {

    public static void main(String[] args) throws IOException {

        //输出流
        FileOutputStream out = new FileOutputStream("001.xlsx");
        try {
            //数据
            List<TestModel> datas = createDatas();

            List<TestModel> datas2 = createDatas2();

            //数据写入器
            IExcelWriter writer = new AnnotationExcelWriterImpl<TestModel>();

            //写入数据
            writer.dataType(TestModel.class).start(out).write(datas).write(datas2).sheetIndex(1).write(datas2).end();

        }finally {
            out.close();
        }

    }

    /**
     * 创建数据
     *
     *
     */
    private static List<TestModel> createDatas() {
        List<TestModel> list = new ArrayList<>();

        list.add(new TestModel("TEST1", "TEST1试试洒水大多", "2018-09-01 10:10:10", BigDecimal.ONE));
        list.add(new TestModel("TEST2", "TEST2", "2018-09-01 10:10:10", BigDecimal.ZERO));
        list.add(new TestModel("TEST3", "TEST3试洒试洒", "2018-09-01 10:10:10", BigDecimal.TEN));

        return list;
    }

    /**
     * 创建数据
     *
     *
     */
    private static List<TestModel> createDatas2() {
        List<TestModel> list = new ArrayList<>();

        list.add(new TestModel("TEST4", "TEST4", "2018-09-01 10:10:10", BigDecimal.ONE));
        list.add(new TestModel("TEST5", "TEST5", "2018-09-01 10:10:10", BigDecimal.ZERO));
        list.add(new TestModel("TEST6", "TEST6", "2018-09-01 10:10:10", BigDecimal.TEN));

        return list;
    }

}
