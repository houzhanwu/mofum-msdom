package com.mofum.msdom.excel.demo.annotation.parser;

import com.mofum.msdom.excel.constant.RowContext;
import com.mofum.msdom.excel.demo.model.TestModel;
import com.mofum.msdom.excel.parser.ExcelParserCallback;

/**
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 18:50
 */
//@Component
public class TestImport implements ExcelParserCallback<TestModel> {

//    @Autowired
//    ObjectMapper objectMapper

    public void parseDataAfter(RowContext<TestModel> testModel) {

        System.out.println(testModel.getData().getName());
//        objectMapper.insert(o);
    }
}
