package com.mofum.msdom.excel.demo.validator;

import com.mofum.msdom.excel.constant.RowContext;
import com.mofum.msdom.excel.demo.model.TestModel;
import com.mofum.msdom.excel.validator.RuntimeValidator;
import com.mofum.msdom.excel.validator.Validator;

/**
 * @author 1615690513@qq.com
 * @since 2018/11/26 0026 16:32
 */
public class ReadValidator implements RuntimeValidator<RowContext<TestModel>> {
    @Override
    public boolean doCheckData(RowContext<TestModel> context) {
        TestModel testModel = context.getData();
        return !"TEST3".equals(testModel.getCode());
    }
}
