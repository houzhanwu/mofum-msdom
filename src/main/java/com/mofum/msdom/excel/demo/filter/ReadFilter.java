package com.mofum.msdom.excel.demo.filter;

import com.mofum.msdom.excel.constant.RowContext;
import com.mofum.msdom.excel.demo.model.TestModel;
import com.mofum.msdom.excel.filter.DataFilter;

public class ReadFilter implements DataFilter<RowContext<TestModel>> {
    @Override
    public boolean execute(RowContext<TestModel> context) {

        TestModel testModel =  context.getData();

        return "TEST4".equals(testModel.getCode());
    }
}
