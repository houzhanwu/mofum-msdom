package com.mofum.msdom.excel.demo.model;

import com.mofum.msdom.excel.annotation.ATColumn;
import com.mofum.msdom.excel.annotation.ATHeaderRow;
import com.mofum.msdom.excel.annotation.ATSheet;
import com.mofum.msdom.excel.converter.impl.javatype.AmountConvert;
import com.mofum.msdom.excel.annotation.ATWorkbook;
import com.mofum.msdom.excel.converter.impl.javatype.DateConverter;
import com.mofum.msdom.excel.demo.filter.ReadFilter;

import java.math.BigDecimal;

/**
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 11:07
 */
@ATWorkbook(name = "TEST 导出", cache = 1, sheets = {
        @ATSheet(name = "S3 工作薄", autoSize = true, index = 3, filters = ReadFilter.class,
                headers = @ATHeaderRow(values = {"名称", "编码", "操作时间", "金额", "创建人"})),
        @ATSheet(name = "S4 工作薄", autoSize = true, index = 4),
})
public class TestModel2 {

    /**
     * 名称
     */
    @ATColumn(index = 0)
    private String name;

    /**
     * 编码
     */
    @ATColumn(index = 1)
    private String code;

    /**
     * 操作时间
     */
    @ATColumn(index = 2)
    private String datetime;

    /**
     * 金额
     */
    @ATColumn(index = 3, converter = AmountConvert.class)
    private BigDecimal amount;

    /**
     * 创建人
     */
    @ATColumn(index = 4)
    private String createBy;

    public TestModel2() {
    }

    public TestModel2(String name, String code, String datetime, BigDecimal amount) {
        this.name = name;
        this.code = code;
        this.datetime = datetime;
        this.amount = amount;
    }

    public TestModel2(String name, String code, String datetime, BigDecimal amount, String createBy) {
        this.name = name;
        this.code = code;
        this.datetime = datetime;
        this.amount = amount;
        this.createBy = createBy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    @Override
    public String toString() {
        return "TestModel2{" +
                "name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", datetime='" + datetime + '\'' +
                ", amount=" + amount +
                ", createBy='" + createBy + '\'' +
                '}';
    }
}
