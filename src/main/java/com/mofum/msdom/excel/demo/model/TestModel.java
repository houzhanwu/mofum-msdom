package com.mofum.msdom.excel.demo.model;

import com.mofum.msdom.excel.annotation.*;
import com.mofum.msdom.excel.converter.impl.javatype.AmountConvert;
import com.mofum.msdom.excel.demo.validator.ReadValidator;

import java.math.BigDecimal;

/**
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 11:07
 */
@ATWorkbook(name = "TEST 导出", cache = 1, sheets = @ATSheet(name = "S1 工作薄", headers = {
        @ATHeaderRow(values = {"名称", "编码", "操作时间", "金额"}, index = 0),
        @ATHeaderRow(values = {"名称", "编码", "操作时间", "金额"}, index = 1),
        @ATHeaderRow(values = {"名称", "编码", "操作时间", "金额"}, index = 2),
        @ATHeaderRow(values = {"测试表格"}, index = 3)

}, ranges = {
        @ATMergeRange(startRow = 0, endRow = 2, startCol = 1, endCol = 1),
        @ATMergeRange(startRow = 0, endRow = 1, startCol = 2, endCol = 2),
        @ATMergeRange(startRow = 1, endRow = 2, startCol = 0, endCol = 0),
        @ATMergeRange(startRow = 3, endRow = 3, startCol = 0, endCol = 3)
})
)
public class TestModel {

    /**
     * 名称
     */
    @ATColumn(index = 0)
    private String name;

    /**
     * 编码
     */
    @ATColumn(index = 1, validator = ReadValidator.class)
    private String code;

    /**
     * 操作时间
     */
    @ATColumn(index = 2,styles = @ATStyle("font-color:red;font-name:微软雅黑"))
    private String datetime;

    /**
     * 金额
     */
    @ATColumn(index = 3, converter = AmountConvert.class)
    private BigDecimal amount;

    public TestModel() {
    }

    public TestModel(String name, String code, String datetime, BigDecimal amount) {
        this.name = name;
        this.code = code;
        this.datetime = datetime;
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "TestModel{" +
                "name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", datetime='" + datetime + '\'' +
                ", amount=" + amount +
                '}';
    }
}
