package com.mofum.msdom.excel.demo.annotation.parser;

import com.mofum.msdom.excel.annotation.parser.impl.ATWorkbookParser;
import com.mofum.msdom.excel.demo.model.TestModel;
import com.mofum.msdom.excel.metadata.MPWorkbook;

import java.lang.reflect.Type;
import java.util.Map;

/**
 * @author 1615690513@qq.com
 * @since 2018/11/21 0021 15:20
 */
public class ATWorkbookParserDemo {

    public static void main(String[] args) {

        ATWorkbookParser<TestModel> atWorkbookParser = new ATWorkbookParser();

        TestModel testModel = new TestModel();

        Map<Type, MPWorkbook> as = atWorkbookParser.parseType(testModel);

        for (Type type : as.keySet()) {

            MPWorkbook workbook = as.get(type);

            System.out.println(type.getClass().toString() + "--->" + workbook.getName());

        }

    }

}
