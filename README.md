# mofum-msdom

#### 项目介绍
    Microsoft 文档操作工具类...
    
    本工具类基于Apache POI 4.0 SAX 用户模式实现。 
    
    操作简单但是不失灵活性。基于Sax模式实现，消耗内存相对较少，有效避免OOM（OutOfMemoryError）内存溢错误。

    读取原理：读数据是基于事件模式，读一条数据，释放一条数据，整个解析过程只占用一条数据解析的内存。
    
    写入原理：写数据则是将部分数据放入内存中，msdom-excel称这样的操作叫做缓存，超过缓存的写入数据部分则根据时间先后存于硬盘上。


#### MAVEN安装方式


```
<dependency>
    <groupId>com.mofum.msdom</groupId>
    <artifactId>mofum-msdom</artifactId>
    <version>1.0.2.3</version>
</dependency>
```

#### 1.0.2.3版本功能概要

- 目前仅支持XLSX(Excel 2007)版本的Excel导入导出。暂不支持CVS,XLS相关的格式。

- 支持必要参数非空校验 required

- 支持过滤器读取（DataFilter）

- 支持自定义验证器（Validator）

- 支持文件（File）读取模式。

- 支持分页写入模式 （PageAnnotationExcelWriterImpl）

- 支持注解写入模式（AnnotationExcelWriterImpl）

- 支持多类型混合追加写入模式（MultiAnnotationExcelWriterImpl）

- 支持自定义Excel类型转换（ExcelConvert）

- 支持追加写入,多Sheet 混合写入模式。

- 支持合并单元格。

- 支持较多的样式

- 支持导出的数据的格式化


#### 1.0.3.0新版本功能预览

- 支持流读取模式（√）

- 支持模板导入导出（-）

- 支持模板注解相关配置（-）

#### 测试

##### 1、Excel 测试说明

    测试示例 见com.mofum.msdom.excel.demo 目录

##### 读取测试：（ReadDemo）

    JDK 版本： 1.8

    系统版本： Windows 7
    
    测试JVM ： -Xms1m -Xmx5m
    
    测试数据 ：100万条数据（20列）见附件（sxxf1.xlsx）
    
    最低JVM内存需求：5M
    

##### 写入测试：(WriteDemo)

    JDK 版本： 1.8

    系统版本： Windows 7
    
    测试JVM ： -Xms1m -Xmx5m

    最低JVM内存需求：5M


#### 软件架构

    基于POI 4.0 开发的 excel 导入导出工具类


#### 使用教程

##### 1.读取操作：

```
 /**
  * 读取测试模型
  */
public class ReadDemo {

    public static void main(String[] args) throws IOException {

        //创建Reader ，对Test模型进行解析
        IExcelReader<TestModel, TestModelCallback> reader = new AnnotationExcelReaderImpl<TestModel, TestModelCallback>();

        //POI 读取Excel 文件
        reader.read(new File("001.xlsx"), TestModel.class, new TestModelCallback());

    }

}
```

[详细操作见WIKI文档](https://gitee.com/mofum/mofum-msdom/wikis/%E5%BF%AB%E9%80%9F%E4%BD%BF%E7%94%A8?sort_id=1053563)

##### 2.写入操作


```

public class WriteDemo {

    public static void main(String[] args) throws IOException {

        //数据写入器
        IExcelWriter writer = new AnnotationExcelWriterImpl<TestModel>();

        //写入数据
        writer.dataType(TestModel.class).start(out).write(datas).end();

    }

}
```



[详细操作见WIKI文档](https://gitee.com/mofum/mofum-msdom/wikis/%E5%BF%AB%E9%80%9F%E4%BD%BF%E7%94%A8?sort_id=1053575)

